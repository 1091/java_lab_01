package lab01;

public interface Action {
	public void run();
	public Boolean isOk();
}
