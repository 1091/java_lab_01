package lab01;

import lejos.nxt.Motor;

public class MoveAction implements Action{
	public void run(){
		Motor.A.setSpeed(1000);
		Motor.B.setSpeed(1000);
		Motor.A.forward();
		Motor.B.forward();
	}
	
	public Boolean isOk() {
		return true;
	}
}


