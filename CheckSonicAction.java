package lab01;

import lejos.nxt.UltrasonicSensor;
import lejos.nxt.SensorPort;

public class CheckSonicAction implements Action {
	private int distance; 
	
	public void run() {
		UltrasonicSensor sensor = new UltrasonicSensor(SensorPort.S1);
		distance = sensor.getDistance();		
	}
	
	public Boolean isOk(){
		if (distance < 50){
			return Boolean.FALSE;
		} else {
			return Boolean.TRUE;
		}
	}
}
