package lab01;

import lejos.nxt.Motor;

public class StopAction implements Action{
	public void run(){
		Motor.A.stop(true);
		Motor.B.stop();
	}
	
	public Boolean isOk() {
		return true;
	}
}
