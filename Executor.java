package lab01;

import java.util.ArrayList;

import lejos.nxt.Button;

public class Executor {
	private ArrayList<Action> actions;
	private Boolean NeedToRotate;
	
	public Executor(){
		NeedToRotate = false;
		actions = new ArrayList<Action>();
		actions.add(new MoveAction());
		actions.add(new StopAction());
		actions.add(new RotateAction());
		actions.add(new CheckSonicAction());
	}
	
	public void execute(){
		while(!Button.ESCAPE.isDown()){
			for (Action action : actions){
				if (action instanceof CheckSonicAction){
					action.run();
					if (!action.isOk()){
						NeedToRotate = true;
					} else {
						NeedToRotate = false;
					}			
				}
				if (action instanceof MoveAction){
					if (!NeedToRotate){
						action.run();
					}
				}
				if (action instanceof RotateAction){
					if (NeedToRotate){
						action.run();
					}
				}
				if (action instanceof StopAction){
					if (NeedToRotate){
						action.run();
					}					
				}
			}
		}
	}
}
