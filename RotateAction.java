package lab01;

import lejos.nxt.Motor;

public class RotateAction implements Action {
	public void run(){
		Motor.A.rotate(450, true);
		Motor.B.rotate(-450);
	}
	
	public Boolean isOk() {
		return true;
	}
}
